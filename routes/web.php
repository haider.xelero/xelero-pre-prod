<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
}); 


//Route::get('/testSendMail', Controller@testSendMail );
//Route::get('/testSendMail', 'ControllerHome@testSendMail');
Route::put('/storeFn', 'ControllerHome@storeFn');

// section jobs
Route::get('/job/{id}/{slug}','ControllerHome@solo');
Route::get('/carriere','ControllerHome@carriere')->name('carriere');
Route::put('/insertCondidature','ControllerHome@insertCondidature');
Route::post('/sendMessage','ControllerHome@sendMessage');

Route::get('/contact','ControllerHome@contact')->name('contact');
Route::put('/storeContact','ControllerHome@storeContact');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
