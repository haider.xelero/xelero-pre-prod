<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Mail</title>


    </head>
    <body>
        <div class="row justify-content-center">
            <div class="col-8">
                <p>
                — English version below — <br>

                    Bonjour {{ $user->prenom }} , <br>

                    Nous vous remercions pour votre candidature pour le poste de {{ $job }}.<br>

                    Nous allons étudier votre candidature avec attention et reviendrons vers vous sous 5 jours : gardez votre téléphone à proximité ! ;)<br>

                    Pour continuer à découvrir XELERO et les nouvelles opportunités, nous vous proposons de visiter régulièrement notre site web www.xelero.io  ou de suivre notre page entreprise LinkedIn https://www.linkedin.com/company/xelero-io <br>

                    A bientôt,<br>

                    Arij de l'équipe Heroes

                </p>
                ———————————————
                <p>
                    Hello {{ $user->prenom }} ,<br>

                    Thank you for your application for the position of {{ $job }}.<br>

                    We will study your application carefully and get back to you within 5 days: keep your phone nearby! ;)<br>

                    To know more about XELERO and all new opportunities, we suggest that you regularly visit our website www.xelero.io or follow our LinkedIn company profile https://www.linkedin.com/company/xelero-io <br>

                    Best regards,<br>

                    Arij from Heroes team
                </p>
                <p>
                    Cordialement <br>
                    Xelero
                </p>
            </div>
        </div>
    </body>
</html>

