<div class="footer" >
    <div class="container py-3">
        <div class="row py-5">
            <div class="col-lg-4 d-flex flex-column py-4 footer-arrow-box">
                <img src="{{URL::asset('assets/images/logo-white-black.svg')}}" class="mt-1" style="max-width: 100px;" >
                <p class="footer-desc my-5 ml-0" > © XELERO 2022. <br>
                All rights reserved.</p>
            </div>
            <div class="col-lg-4 d-flex flex-column py-4">
                <h5 class="footer-titre" >Office</h5>
                <p class="footer-desc mb-4  ml-0" >App 4, 2 Rue Ibrahim Jaffel - El Menzah 4 - <br>1002 Tunis, TUNISIA</p>
                <h5 class="footer-titre" >Follow</h5>
                <div class="d-flex mt-3">
                    <a href="https://www.linkedin.com/company/xelero-io/"  target="_blank" class="my-auto">
                        <img src="{{URL::asset('assets/images/icon-in.svg')}}" class="">
                    </a>
                    <a href="https://www.facebook.com/Xeleroio-102716124994279" target="_blank" class="my-auto mx-4">
                        <img src="{{URL::asset('assets/images/icon-fb.svg')}}" class="">
                    </a>
                    <a href="https://www.instagram.com/xelero.io/" target="_blank" class="my-auto">
                        <img src="{{URL::asset('assets/images/icon-insta.svg')}}" class="" >
                    </a>
                </div>
            </div>

            <div class="col-lg-4 d-flex flex-column py-4">
                <p>
                    <a class="footer-desc" href="{{URL::asset('/')}}" id="savoirFaire">Savoir faire</a>
                </p>
                <p>
                    <a class="footer-desc" href="{{URL::asset('/#section-three')}}" >Cas client</a>
                </p>
                <p>
                    <a href="{{route('carriere')}}" class="footer-desc" >Carrière</a>
                </p>
                <p>
                    <a href="{{route('contact')}}"  class="footer-desc" >Contactez-nous</a>
                </p>
            </div>
        </div>
    </div>
</div>
