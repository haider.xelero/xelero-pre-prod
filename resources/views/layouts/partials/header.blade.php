<!-- Header -->
<div class="header" id="nav-header">
    <nav class="container navbar navbar-expand-lg navbar-light py-2 py-md-4">
        <a class="navbar-brand" href="/"><img src="{{URL::asset('/assets/images/logo.svg')}}" class="m-auto logo-header" ></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" style="border: 0;" >
            <img class="" src="{{URL::asset('/assets/images/menu.svg')}}" />
        </button>
        <div class="collapse navbar-collapse" id="navbarNav" >
            <ul class="navbar-nav d-flex" style="" >
                <li class="nav-item dropdown d-flex flex-column">
                    <a class="my-auto nav-link dropdown-toggle titre-item-menu" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                        Savoir faire
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{URL::asset('/#xelero_creative')}}">Xelero Creatives</a>
                        <a class="dropdown-item" href="{{URL::asset('/#xelero_transformes')}}">Xelero Transformers</a>
                        <a class="dropdown-item" href="{{URL::asset('/#xelero_heroes')}}">Xelero Heroes</a>
                    </div>
                </li>
                <li class="nav-item d-flex" >
                    <a class="nav-link titre-item-menu m-auto" href="{{URL::asset('/#section-three')}}">Cas client</a>
                </li>
                <li class="nav-item d-flex" >
                    <!--<a class="nav-link titre-item-menu m-auto" data-toggle="modal" data-target="#ModalCarriere" href="">Carrière</a> commented by Habib Aroua -->
                    <a class="nav-link titre-item-menu m-auto"  href="{{route('carriere')}}">Carrière</a>
                </li>
                <li class="nav-item d-flex"  >
                    <a href="{{route('contact')}}" class="nav-link button-contact m-auto">
                        Contactez-nous
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<!-- Header -->
