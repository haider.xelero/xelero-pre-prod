<!-- 
    <div class="head-footer" >
        <div class="container py-3">
            <div class="row py-5">
                <div class="col-9 col-lg-8 d-flex flex-column">
                    <h1 class="mb-5" >Démarrons votre projet <br> innovant  ensemble, parlez nous de votre besoin !</h1>
                    <p>
                        Toute notre équipe est à votre écoute, n’hésitez pas à nous contacter                
                    </p>
                </div>
                <div class="col-3 col-lg-4 d-flex p-0 p-md-auto" id="box-footer-arrow" >
                    <img src="{{URL::asset('assets/images/footer-arrow.svg')}}" class="mx-auto arrow-footer"  id="footer-arrow"  >
                    <img src="{{URL::asset('assets/images/footer-arrow-hover.svg')}}" class="mx-auto arrow-footer" id="footer-arrow-hover" style="display:none" >
                </div>
                <div class="col-lg-12">
                    <hr>
                </div>
                <div class="col-lg-12">
                    <div class="row  justify-content-between">
                        <div class="col-md-6 col-lg-6 d-flex flex-column">
                            <label class="my-1 my-md-4" >Email</label>
                            <a  class="" href="mailto:Contact@xelero.io">Contact@xelero.io</a>
                        </div>
                        <div class="col-md-4 col-lg-3 d-flex flex-column">
                            <label class="mt-4 mb-1 mt-md-4 mb-md-4 " >Phone</label>
                            <a  class="" href="tel:+216 31 558 906">+216 31 558 906</a>
                        </div>
                    </div>
                </div> 

            </div>
        </div>
    </div> 
-->


 <!-- header stuff  -->


        
        <!-- Modal Contact Us-->
            <!-- <div class="modal fade" id="ModalContactUs" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content p-5">
                    <div class="d-flex flex-row px-5 py-3" >
                            <h1 class="my-auto ml-auto titre-modal titre-carriere" >Contact</h1>
                            <button type="button" class="close ml-auto my-auto" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" >
                                    <img src="{{URL::asset('/assets/images/Icon material-close.svg')}}">
                                </span>
                            </button>
                        </div>
                    <div class="modal-body p-4">

                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>

                        <script>

                            hbspt.forms.create({

                                region: "na1",

                                portalId: "9467376",

                                formId: "f80fabfc-c2e8-4125-922c-7a034772d86a"

                            });

                        </script>
                    </div>

                    </div>
                </div>
            </div>  -->
        <!-- Modal Contact Us -->

        <!-- Modal Carriere -->
        <!--<div class="modal fade" id="ModalCarriere" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content pt-5 px-3">
                    <div class="d-flex flex-row px-0 px-md-5 py-3" >
                        <h1 class="my-auto ml-auto titre-modal titre-carriere" >Candidature spontané</h1>
                        <button type="button" class="close ml-auto my-auto" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" >
                                <img src="{{URL::asset('/assets/images/Icon material-close.svg')}}">
                            </span>
                        </button>
                    </div>
                <div class="modal-body px-0 px-md-5 py-4">
                    <form method="post" action="/insertCondidature" enctype="mutlipart/form-data" >
                    {{csrf_field()}}
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <label class="label-carriere" >
                                    Prénom
                                    <input type="text"  name="prenom" class="input-carriere" required  >
                                </label>
                            </div>
                            <div class="col-12 col-lg-6">
                                <label class="label-carriere" >
                                    Nom
                                    <input type="text" name="nom" class="input-carriere" required >
                                </label>
                            </div>
                            <div class="col-12 col-lg-12">
                                <label class="label-carriere" >
                                    Email
                                    <input type="text" name="email" class="input-carriere" required >
                                </label>
                            </div>
                            <div class="col-12 col-lg-12">
                                <label class="label-carriere" >
                                    Lettre de motivation
                                    <textarea class="input-carriere" name="letter" style="height:150px" required  ></textarea>
                                </label>
                            </div>
                            <div class="col-12 col-lg-12">

                                <div class="alert alert-danger" id="alert" role="alert" style="display:none;text-align:center ;" >
                                    Merci de télécharger votre CV
                                </div>

                                <label for="file-upload" class="custom-file-upload d-flex mt-3">
                                <div class="d-flex m-auto w-75"  style="position:relative">
                                    <img src="{{URL::asset('assets/images/file.svg')}}" class="my-auto ml-auto mr-3 file-icon">
                                    <p class="m-auto" id="resumeFile" >Attacher votre CV</p>
                                </div>
                                </label>
                                <input id="file-upload" name="cv" type="file"  class="d-none" />
                            </div>
                            <div class="col-12 col-lg-12 d-flex mt-3">
                                <button  class="button-contact px-5 m-auto" id="send" >Envoyer</button>
                            </div>

                        </div>
                        <input type="hidden" name="_method" value="PUT">
                    </form>
                </div>

                </div>
            </div>
        </div>-->
        <!-- Modal Carriere -->




 <!-- header stuff  -->