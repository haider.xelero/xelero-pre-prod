<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head >

        <meta charset="utf-8">
        <!-- icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="{{URL::asset('assets/images/x-2.svg')}}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <meta property='og:title' content='XELERO - Xelerate your digital transformation'/>
        <meta property='og:image' content='https://xelero.io/assets/images/xelero_transformers.png'/>
        <meta property='og:description' content='Your best partner in UX/UI design, Web and mobile development, advisroy and project management'/>

        <!-- Style CSS -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/style.css') }}">

        <!-- Bootstrap  CSS -->
        <!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> -->
        <link rel="stylesheet" href="{{URL::asset('assets/bootstrap/bootstrap.min.css') }}" >

        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="{{URL::asset('assets/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('assets/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css')}}">
        <!-- <link rel="stylesheet" href="{{URL::asset('assets/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('assets/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css')}}"> -->

        <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script> -->

        <title>XELERO - Xelerate your digital transformation</title>

        <!-- SweetAlert JS -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    </head>
    <body>
      @include('layouts.partials.header')
      @yield('content')
      @yield('home')
      @yield('carriere')
      @yield('solo')
      @yield('404')
      @include('layouts.partials.footer')


      <!-- Bootstrap JS -->
      <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
      <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> the truth -->
      <script src="{{URL::asset('assets/js/jquery.min.js')}}"></script> <!-- added in 20/01/2022 -->
      <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script> the truth -->
      <script src="{{URL::asset('assets/bootstrap/bootstrap.bundle.min.js')}}"></script> <!-- added in 20/01/2022 -->

      <!-- Owl Carousel JS -->
      <script script src="{{URL::asset('assets/OwlCarousel2-2.3.4/dist/owl.carousel.min.js')}}"></script>

      <script src="{{URL::asset('assets/js/header.js')}}"></script>
      <script src="{{URL::asset('assets/js/footer.js')}}"></script>

      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-187475384-1"></script>
      <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-187475384-1');
      </script>

      <script src="{{URL::asset('assets/js/file.js')}}"></script>
    </body>
</html>
