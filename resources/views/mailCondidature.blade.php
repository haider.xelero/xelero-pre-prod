<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Mail</title>


    </head>
    <body>
        <div class="row justify-content-center">
            <div class="col-8">
                <p>
                — English version below — <br>

                    Bonjour {{ $user->prenom }} , <br>

                    L'équipe XELERO vous remercie pour votre intérêt et votre confiance.<br>
                    Nous vous informons que nous avons bien reçu votre candidature spontanée. <br>
                    Nous revenons vers vous quand la meilleure occasion professionnelle conforme à votre profil se présentera.<br>

                    A bientôt,<br>

                    Arij de l'équipe Heros

                </p>
                ———————————————
                <p>
                    Hello {{ $user->prenom }} ,<br>

                    XELERO Team thank you for your interest and your trust.<br>

                    We inform you that we have received your resume. <br>
                    We'll get back to you when we find the best opportunity that matches your profile.<br>

                    See you soon <br>

                    Arij from Heroes team
                </p>
                <p>
                    Cordialement <br>
                    Xelero
                </p>
            </div>
        </div>
    </body>
</html>

