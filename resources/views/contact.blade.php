@extends('layouts.main')
@section('content')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<!-- Begin Section -->
<section class="section-page-contact py-5" style="position:relative;">

    @if ($message = session('success'))
    <div class="alert alert-success alert-block alert-contact">
        <strong class="my-auto mr-auto">{{ $message }}</strong>
        <button type="button" class="close mb-auto ml-auto" data-dismiss="alert">×</button>	
    </div>
    @endif

    @if ($message = session('error'))
    <div class="alert alert-warning alert-block alert-contact">
        <strong class="my-auto mr-auto">{{ $message }}</strong>
        <button type="button" class="close mb-auto ml-auto" data-dismiss="alert">×</button>	
    </div>
    @endif
    <div class="container-lg">
        <div class="row mx-0 justify-content-between inverse-column" id="" >
            <div class="col-12 col-sm-12 col-md-7 col-lg-7 d-flex p-0" >
                <iframe class="box-map-contact my-auto mr-auto" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3192.888485454025!2d10.180738715499459!3d36.845146679939326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12fd35e7f6fb8335%3A0x7d5b7a514df59c7d!2sXELERO!5e0!3m2!1sfr!2stn!4v1638195672956!5m2!1sfr!2stn" width="600" height="530" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
            <div class="col-12 col-sm-12 col-md-5 col-lg-5 d-flex p-0" >
                <div class="modal-body">
                    <h2 class="titre-contact-page mb-3">Parlez-nous de votre besoin</h2>
                    <p class="sous-titre-contact-page">Merci d’avance pour votre intérêt, n’hésitez pas à nous laisser un message via ce formulaire.<br>
                    Nous revenons vers vous rapidement !</p>
                    <form method="post" action="/storeContact" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <label class="label-carriere" >
                                    Prénom
                                    <input type="text" name="prenom" class="input-carriere" required>
                                </label>
                            </div>
                            <div class="col-12 col-lg-6">
                                <label class="label-carriere" >
                                    Nom
                                    <input type="text" name="nom" class="input-carriere" required>
                                </label>
                            </div>
                            <div class="col-12 col-lg-6">
                                <label class="label-carriere" >
                                    Téléphone
                                    <input type="tel" name="tel" class="input-carriere"  required> <!-- min="0" max="8" -->
                                </label>
                            </div>
                            <div class="col-12 col-lg-6">
                                <label class="label-carriere" >
                                    Email
                                    <input type="email" name="email" class="input-carriere" required>
                                </label>
                            </div>
                            <div class="col-12 col-lg-12">
                                <label class="label-carriere" >
                                    Objet
                                    <input type="text" name="objet" class="input-carriere" required>
                                </label>
                            </div>
                            <div class="col-12 col-lg-12">
                                <label class="label-carriere" >
                                    Message
                                    <textarea class="input-carriere" name="letter" style="height:150px" required></textarea>
                                </label>
                            </div>
                            <div class="col-12 col-lg-12">
                                <div class="g-recaptcha" data-sitekey="6Ld3wb4eAAAAAJ__d9OzI5AIA4CmqI3KYVX66Ple"></div>
                            </div>
                            <div class="col-12 col-lg-12 d-flex mt-3">
                                <button  class="button-contact px-5 m-auto" id="send" >Envoyer</button>
                            </div>
                        </div>
                        <input type="hidden" name="_method" value="PUT">
                    </form>
                </div>
            </div>
        </div>
        <div class="row mx-0 mb-5 justify-content-center w-100">
            <div class="col-12 col-lg-12 px-0 px-md-3">
                <h1 class="titre-word-office mt-4 mt-md-0 mb-3">Our worldwide offices</h1>
                <div class="row justify-content-between mx-0">
                    <!-- Tunisia -->
                    <div class="col-12 col-lg-4">
                        <a href="tel:+21631633344" class="d-flex box-word-office my-1 my-md-0">
                            <div class="d-flex flex-column my-auto">
                                <p class="titre-contact-word-office mt-auto mb-1">Tunisia</p>
                                <p class="sous-titre-location-contact-word-office mb-auto mt-1"> El Menzah 4, Tunis </p>
                                <p class="sous-titre-contact-word-office mb-auto"> +216 31 633 344 </p>
                            </div>
                            <img src="{{URL::asset('/assets/images/arrow-contact.png')}}" class="my-auto ml-auto contact-img1">
                        </a>
                    </div>
                    <!-- Tunisia -->

                    <!-- France -->
                    <div class="col-12 col-lg-4">
                        <a href="tel:+33682042065" class="d-flex  box-word-office my-1 my-md-0">
                            <div class="d-flex flex-column my-auto">
                                <p class="titre-contact-word-office mt-auto mb-1">France</p>
                                <p class="sous-titre-location-contact-word-office mb-auto mt-1">Belle de Maie, Marseille</p>
                                <p class="sous-titre-contact-word-office mb-auto">+33 6 82 04 20 65</p>
                            </div>
                            <img src="{{URL::asset('/assets/images/arrow-contact.png')}}" class="my-auto ml-auto contact-img1">
                        </a>
                    </div>
                    <!-- France -->

                    <!-- Canada -->
                    <div  class="col-12 col-lg-4">
                        <a href="tel:+14187145079" class="d-flex  box-word-office my-1 my-md-0">
                            <div class="d-flex flex-column my-auto">
                                <p class="titre-contact-word-office mt-auto mb-1">Canada</p>
                                <p class="sous-titre-location-contact-word-office mb-auto mt-1">Témiscouata-sur-le-Lac, Québec</p>
                                <p class="sous-titre-contact-word-office mb-auto">+1 418 714 5079</p>
                            </div>
                            <img src="{{URL::asset('/assets/images/arrow-contact.png')}}" class="my-auto ml-auto contact-img1">
                        </a>
                    </div>
                    <!-- Canada -->


                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section -->
@section('carriere')
    @if(session()->has('message'))
    <script>
        Swal.fire
        (
            '',
            'Votre demande a été enregistrée',
            'success'
        )
    </script>
@endif
@if(session()->has('error'))
    <script>
        Swal.fire
        (
            '',
            'Veuillez confirmer que vous pas un robot',
            'error'
        )
    </script>
@endif
@endsection



