@extends('layouts.main')
@section('home')

<!-- Cas client= case study, contact us = contacter nous -->

<!-- Begin Section One Banner -->
<section class="section-one"  id="section-one">
    <div class="container-lg">
        <div class="row mx-0 inverse-column" id="section-one-slider" >
            <div class="col-12 col-sm-12 col-md-5 col-lg-5 d-flex" >
                <div class="row  my-auto">
                    <div class="col-lg-12"><h3 class="mb-3" >We are a Digital agency</h3></div>
                    <div class="col-lg-12"><h2 class="my-2" >Accélérez votre <br> passage au digital avec </h2></div>
                    <div class="col-lg-12"><h1 class="mb-3" >XELERO</h1></div>
                    <div class="col-lg-12">
                        <div class="row my-3 " >
                            <div class="col-6 d-flex">
                                <a href="#xelero_savoir" class="button-contact">Nous connaitre</a>
                            </div>
                            <div class="col-6 d-flex">
                                <!--<a href="{{URL::asset('/contact')}}" class="button-rdv" data-toggle="modal" data-target="#ModalContactUs">Prendre RDV</a> -->
                                <a href="https://calendly.com/aymentouhent/30min" class="button-rdv" >
                                    Prendre RDV
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-7 col-lg-7 d-flex " >
                <img src="{{URL::asset('/assets/images/banner.png')}}" class="my-auto ml-auto w-100" >
            </div>
        </div>
    </div>
</section>
<!-- End Section One Banner -->

<!-- Begin Section Notre savoir faire -->
<section class="section-two-white" id="xelero_savoir">
    <div class="container py-5">
        <div class="row">
            <div class="col-md-7 col-lg-7 d-flex flex-column ">

                <p class="titre-section-two my-5" >Notre savoir faire</p>

                <p class="desc-creatives">
                    Xelero vous accompagne dans la transformation <br>
                    de vos activités en tirant profit de toute la puissance du digital.
                </p>

                <p class="desc-creatives mb-5">
                    Fort en expérience et d’une grande expertise dans <br>
                    différents secteurs d’activités,  nous sommes reconnus par  <br>
                    nos clients ayant répondus à l’ensemble de leurs besoins,<br>
                    de la vision à l’action en consulting, transformation <br>
                    digitale ainsi qu’en agilité de sourcing.
                </p>
            </div>
            <div class="col-md-5 col-lg-5 d-flex flex-column ">
                <img class="mx-auto mt-5 image-savoir-faire" src="{{URL::asset('/assets/images/savoir.png')}}" >
            </div>
        </div>
    </div>
</section>
<!-- End Section Notre savoir faire -->

<!-- Begin Section Xelero CREATIVES -->
<section class="section-two-blue" id="xelero_creative">
    <div class="container py-5">
        <div class="row">
            <div class="col-md-6 col-lg-6 d-flex flex-column image-creatives" >

            <p class="titre-creatives my-5" >XELERO <br>CREATIVES</p>
            <p class="desc-creatives mb-0 mb-md-5  text-white" >
                    Xelero Creatives est notre studio de création
                    dont la mission est de sublimer votre marque et
                    vous donner les moyens d’atteindre vos
                    objectifs. <br>
                    Notre recette ? Allier créativité et
                    expertise technique pour des résultats visibles.
                </p>
                <!-- <img class="arrow-section-two" src="assets/images/flech.svg"> -->
            </div>
            <div class="col-md-6 col-lg-6 d-flex flex-column ">
                    <div class="d-flex flex-column mb-3 mt-5 box-hover mt-0 mt-md-5" >
                        <p class="titre-brand m-0" >Branding</p>
                        <hr class="hr-brand" >
                        <p class="desc-brand" >
                            Nous appuyons le positionnement de votre marque avec une identité visuelle qui vous ressemble
                        </p>
                    </div>
                    <div class="d-flex flex-column mb-3 box-hover" >
                        <p class="titre-brand m-0" >Site web & application mobile</p>
                        <hr class="hr-brand" >
                        <p class="desc-brand" >
                            Nous développons votre site web sur-mesure en prenant en compte tous les  paramètres : le
                            parcours client, l’expérience utilisateur, l’optimisation technique, l’usage mobile pour un résultat  optimal.
                        </p>
                    </div>
                    <div class="d-flex flex-column mb-3 box-hover" >
                        <p class="titre-brand m-0" >Marketing Digital</p>
                        <hr class="hr-brand" >
                        <p class="desc-brand" >
                            Nous déployons votre visibilité afin de vous générer de la notoriété, du trafic qualifiés ainsi que
                            l’acquisition des clients fidèles. Ce que nous faisons : SEO, SEA,  gestion Social Media, Lead
                            génération…
                        </p>
                    </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section Xelero CREATIVES -->

<!-- Begin Section Xelero TRANSFORMERS -->
<section class="section-two-white" id="xelero_transformes">
    <div class="container py-5" >
        <div class="row" >
            <div class="col-lg-7 d-flex flex-column py-5">
                <p class="titre-section-two my-5" >XELERO <br> TRANSFORMERS</p>

                <p class="desc-creatives">
                    Xelero Transformers est la combinaison parfaite entre notre profonde
                    connaissance du business et de la technologie, nous
                    construisons des expériences digitales pouvant rendre
                    votre entreprise plus productive et plus rentables.
                </p>

                <p class="desc-creatives mb-5">
                    Si vous avez besoin d’aide pour aborder votre transformation digitale
                    et faire évoluer votre organisation, nous sommes là pour vous.
                </p>
            </div>
            <div class="col-lg-5 d-flex">
                <img src="{{URL::asset('/assets/images/xelero_transformers.png')}}" class="m-auto w-100">
            </div>
            
            <!-- Begin Notre Process -->
            <div class="col-lg-12 d-flex mt-5 section-two-white">
                <h1 class="box-heroes-h1" >Notre Process</h1>
            </div>
            <div class="col-lg-12 d-flex flex-row mt-5 section-two-white"  style="flex-wrap: wrap;width:100%">
                <div class="size-box  d-flex flex-column" >
                    <div class="d-flex">
                        <p class="size-box-titre mr-auto my-auto" >01</p>
                        <!-- <img class="mr-auto ml-4 my-auto" src="assets/images/flech-white-hover.svg"> -->
                        <!-- <img class="mr-auto ml-4 my-auto" src="assets/images/flech-white.svg"> -->
                        <svg class="mr-auto ml-4 my-auto" xmlns="http://www.w3.org/2000/svg" width="36.704" height="60" viewBox="0 0 36.704 60">
                            <path class="path"  data-name="Path 52" d="M464.56,385.092l-7.47,10.059-1.944,2.618-1.944,2.617-10.595,14.266a1.092,1.092,0,0,1-.878.44H428.381a.524.524,0,0,1-.421-.836l17.772-23.929,1.944-2.617,1.944-2.617v0l-1.944-2.617s2.985,4.02-1.944-2.617L427.96,355.928a.524.524,0,0,1,.421-.836H441.73a1.093,1.093,0,0,1,.878.442L453.2,369.8l1.944,2.617,1.944,2.618Z" transform="translate(-427.856 -355.092)" fill="#F3385A"/>
                        </svg>

                    </div>
                    <p class="size-box-desc mt-2">
                        Audit, <br>
                        compréhension <br>
                        et analyse du besoin
                    </p>
                </div>

                <div class="size-box  d-flex flex-column" >
                    <div class="d-flex">
                        <p class="size-box-titre mr-auto my-auto" >02</p>
                        <!-- <img class="mr-auto ml-4 my-auto" src="assets/images/flech-white.svg"> -->
                        <svg class="mr-auto ml-4 my-auto" xmlns="http://www.w3.org/2000/svg" width="36.704" height="60" viewBox="0 0 36.704 60">
                            <path class="path" data-name="Path 52" d="M464.56,385.092l-7.47,10.059-1.944,2.618-1.944,2.617-10.595,14.266a1.092,1.092,0,0,1-.878.44H428.381a.524.524,0,0,1-.421-.836l17.772-23.929,1.944-2.617,1.944-2.617v0l-1.944-2.617s2.985,4.02-1.944-2.617L427.96,355.928a.524.524,0,0,1,.421-.836H441.73a1.093,1.093,0,0,1,.878.442L453.2,369.8l1.944,2.617,1.944,2.618Z" transform="translate(-427.856 -355.092)" fill="#F3385A"/>
                        </svg>
                    </div>
                    <p class="size-box-desc mt-2">
                        Identification <br>
                        des aspects <br>
                        fonctionnels et <br>
                        du cadre technique
                    </p>
                </div>

                <div class="size-box  d-flex flex-column" >
                    <div class="d-flex">
                        <p class="size-box-titre mr-auto my-auto" >03</p>
                        <!-- <img class="mr-auto ml-4 my-auto" src="assets/images/flech-white.svg"> -->
                        <svg class="mr-auto ml-4 my-auto" xmlns="http://www.w3.org/2000/svg" width="36.704" height="60" viewBox="0 0 36.704 60">
                            <path class="path" data-name="Path 52" d="M464.56,385.092l-7.47,10.059-1.944,2.618-1.944,2.617-10.595,14.266a1.092,1.092,0,0,1-.878.44H428.381a.524.524,0,0,1-.421-.836l17.772-23.929,1.944-2.617,1.944-2.617v0l-1.944-2.617s2.985,4.02-1.944-2.617L427.96,355.928a.524.524,0,0,1,.421-.836H441.73a1.093,1.093,0,0,1,.878.442L453.2,369.8l1.944,2.617,1.944,2.618Z" transform="translate(-427.856 -355.092)" fill="#F3385A"/>
                        </svg>
                    </div>
                    <p class="size-box-desc mt-2">
                        Rédaction du cahier <br>
                        des charges
                    </p>
                </div>

                <div class="size-box  d-flex flex-column" >
                    <div class="d-flex">
                        <p class="size-box-titre mr-auto my-auto" >04</p>
                        <!-- <img class="mr-auto ml-4 my-auto" src="assets/images/flech-white.svg"> -->
                        <svg class="mr-auto ml-4 my-auto" xmlns="http://www.w3.org/2000/svg" width="36.704" height="60" viewBox="0 0 36.704 60">
                            <path class="path" data-name="Path 52" d="M464.56,385.092l-7.47,10.059-1.944,2.618-1.944,2.617-10.595,14.266a1.092,1.092,0,0,1-.878.44H428.381a.524.524,0,0,1-.421-.836l17.772-23.929,1.944-2.617,1.944-2.617v0l-1.944-2.617s2.985,4.02-1.944-2.617L427.96,355.928a.524.524,0,0,1,.421-.836H441.73a1.093,1.093,0,0,1,.878.442L453.2,369.8l1.944,2.617,1.944,2.618Z" transform="translate(-427.856 -355.092)" fill="#F3385A"/>
                        </svg>
                    </div>
                    <p class="size-box-desc mt-2">
                        Conception <br>
                        technique, <br>
                        design CX, UX et UI
                    </p>
                </div>

                <div class="size-box  d-flex flex-column" >
                    <div class="d-flex">
                        <p class="size-box-titre mr-auto my-auto" >05</p>
                    </div>
                    <p class="size-box-desc mt-2">
                        Développement, <br>
                        integration et mise <br>
                        en place du projet
                    </p>
                </div>
            </div>
            <!-- Begin Notre Process -->
        </div>
    </div>
</section>
<!-- End Section Xelero TRANSFORMERS -->

<!-- Begin Section Xelero HEROES -->
<section class="section-two-blue" id="xelero_heroes">
    <div class="container py-5">
        <div class="row">
            <div class="col-12 col-md-8 col-lg-6 py-5">
                <p class="titre-section-two my-5" >XELERO <br> HEROES</p>

                <p class="desc-creatives my-5 text-white">
                    Xelero Heroes , vous  ouvre l’accès aux meilleurs <br>
                    talents techniques & digitaux, ce qui rend le <br>
                    processus de création de votre équipe <br>
                    extrêmement efficace et rentable.
                </p>

                <p class="desc-creatives mb-5 text-white">
                    Besoin de talents permanents ou temporaires ? <br>
                    nous vous accompagnons durant toutes les étapes du sourcing.
                </p>
            </div>

            <div class="col-12 col-md-12 col-lg-6  d-flex" >
                <img src="{{URL::asset('/assets/images/hero.svg')}}" class="m-auto w-100">
            </div>

            <div class="col-lg-12 p-0 mt-3 section-two-blue">
                <div class="row m-auto box-heroes-inside d-flex p-0 mt-5">
                    <div class="col-6 col-md-3 d-flex">
                        <div class="m-auto" >
                            <h1 class="box-heroes-h1" >100%</h1>
                            <h2 class="box-heroes-h2" >DIGITAL</h2>
                        </div>

                    </div>
                    <div class="col-6 col-md-3 d-flex">
                        <div class="m-auto" >
                            <h1 class="box-heroes-h1">16 ans</h1>
                            <h2 class="box-heroes-h2">D'EXPERTISE</h2>
                        </div>
                    </div>

                    <div class="col-6 col-md-3 d-flex">
                        <div class="m-auto" >
                            <h1 class="box-heroes-h1">98%</h1>
                            <h2 class="box-heroes-h2">DE CLIENTS <br> SATISFAITS</h2>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 d-flex">
                        <div class="m-auto" >
                            <h1 class="box-heroes-h1">0</h1>
                            <h2 class="box-heroes-h2">BILAN <br> EN RETARD</h2>
                        </div>
                    </div>
                </div>
                <!-- <div class="d-flex w-100">
                    <div class="my-auto" >
                        <h1 class="box-heroes-h1" >100%</h1>
                        <h2 class="box-heroes-h2" >DIGITAL</h2>
                    </div>
                    <div class="ml-auto my-auto" >
                        <h1 class="box-heroes-h1">16 ans</h1>
                        <h2 class="box-heroes-h2">D'EXPERTISE</h2>
                    </div>
                    <div class="ml-auto my-auto" >
                        <h1 class="box-heroes-h1">98%</h1>
                        <h2 class="box-heroes-h2">DE CLIENTS <br> SATISFAITS</h2>
                    </div>
                    <div class="ml-auto my-auto" >
                        <h1 class="box-heroes-h1">0</h1>
                        <h2 class="box-heroes-h2">BILAN <br> EN RETARD</h2>
                    </div>
                </div> -->

                </div>
            </div>
        </div>
    </div>
    <!-- Begin image slide -->
    <!-- <div class="col-lg-12 d-flex mt-5">
        <img class="m-auto w-100" src="{{URL::asset('/assets/images/slide.svg')}}" id="image-home-desktop" >
        <img class="m-auto w-100" src="{{URL::asset('/assets/images/slide-mobile.svg')}}" id="image-home-mobile" style="display:none">
    </div> -->
    <!-- End image slide -->
</section>
<!-- End Section Xelero HEROES -->

<!-- Begin Section Three Xelero -->
<section class="container-lg section-three" id="section-three">

    <div class="py-0 py-md-5">
        <!-- Begin Section Notre savoir faire -->
        <div class="row">
            <div class="col-md-8 col-lg-12 d-flex flex-column">
                <p class="titre-section-two my-5" >Fiers de nos clients</p>
               <!-- <p class="desc-section-three">
                    Lorem ipsum dolor sit amet, cum rebum deserunt partiendo te, amet <br>
                    pertinacia contentiones ut sea.
                </p> -->
            </div>
        </div>
        <!-- End Section Notre savoir faire -->

        <!-- Begin Section slider -->
        <div class="row my-1 my-md-5 inverse-column" >

            <div class="col-md-4 col-lg-4 d-flex flex-column " >
                <div class="d-flex flex-column mt-auto"  id="block"></div>
                <div id="dotsOwl" class="mb-auto  d-none d-md-block"></div>
            </div>

            <div class="col-md-8 col-lg-8">
                <div class="owl-carousel owl-theme" id="owl-carousel" >
                    <!-- Carousel 1 (Mall of sousse) -->
                    <div>
                        <img src="{{URL::asset('/assets/images/Case-studies/Mallofsousse.jpg')}}" class="image-slider" >
                        <div id="0" class="d-none">
                            <h1 class="titre-slider mt-auto" id="s1" >Mall of sousse </h1>
                            <!-- <h2 class="sous-titre-slider" >UX/UI design, Mobile App, Wifi</h2> -->
                            <p class="desc-slider mt-4" >
                                Xelero a relevé de défi de la transition digital pour le client Mall of Sousse à travers une expérience digitale 360 ° tels que:
                                <ul>
                                    <li>La conception du site web et son référencement naturel </li>
                                    <li>Mise en place du wifi gratuit </li>
                                    <li>Un potail actif </li>
                                    <li>Le Marketing de proximité avec la technologie iBeacon</li>
                                </ul>
                            </p>
                            <p class="desc-slider">
                                L’objectif est d’assurer la collecte de la data nécessaire qui sera utilisée pour étudier le comportement et les habitudes des clients afin de proposer les meilleures offres aux clients de Mall Of Sousse.
                            </p>
                            <a  href="" class="link-slider mb-5">
                                <!-- Case study <img  class="ml-3" src="{{URL::asset('/assets/images/arrow.svg')}}">  -->
                            </a>
                            <!-- <div id="dotsOwl" class="mb-auto"></div> -->
                        </div>
                    </div>
                    <!-- Carousel 1 (Mall of sousse) -->

                    <!-- Carousel 2 (G5 SAHEL) -->
                    <div>
                        <img src="{{URL::asset('/assets/images/Case-studies/G5-Sahel.jpg')}}" class="image-slider">
                        <div id="1" class="d-none">
                            <h1 class="titre-slider mt-auto" id="s1" >G5 SAHEL</h1>
                            <!-- <h2 class="sous-titre-slider" >UX/UI design, Mobile App, Wifi</h2> -->
                            <p class="desc-slider mt-4" >
                                Xelero a accompagné G5 SAHEL à la réalisation des travaux suivant:
                                <ul>
                                    <li>Branding: Refonte du logo ainsi que toute la charte graphique</li>
                                    <li>Site Web: conception site web dynamique</li>
                                    <li>Mise en place d’une Platforme de collaboration (partage de fichier, chat, vidéo conférence, email.)</li>
                                </ul>
                            </p>
                            <a  href="" class="link-slider mb-5">
                                <!-- Case study <img  class="ml-3" src="{{URL::asset('/assets/images/arrow.svg')}}">  -->
                            </a>
                        </div>
                    </div>
                    <!-- Carousel 2 (G5 SAHEL) -->

                    <!-- Carousel 3 (Smart INOV) -->
                    <div>
                        <img src="{{URL::asset('/assets/images/Case-studies/SmartINOV.jpg')}}" class="image-slider">
                        <div id="2" class="d-none">
                            <h1 class="titre-slider mt-auto" id="s1" >Smart INOV</h1>
                            <!-- <h2 class="sous-titre-slider" >UX/UI design, Mobile App, Wifi</h2> -->
                            <p class="desc-slider mt-4" >
                                Smart Inov a fait appel à Xelero pour les prestations suivantes:
                                <ul>
                                    <li>La refonte de son image de marque </li>
                                    <li>La conception de spécifications techniques et Gestion de projet Site Web (Laravel, ReactJS) .</li>
                                    <li>Cours eLearning motion design </li>
                                    <li>Sécurité Architecture haute disponibilité</li>
                                </ul>
                            </p>
                            <a  href="" class="link-slider mb-5">
                                <!-- Case study <img  class="ml-3" src="{{URL::asset('/assets/images/arrow.svg')}}">  -->
                            </a>
                        </div>
                    </div>
                    <!-- Carousel 3 (Smart INOV) -->

                    <!-- Carousel 4 (SUZUKI) -->
                    <div>
                        <img src="{{URL::asset('/assets/images/Case-studies/SUZUKI.jpg')}}" class="image-slider">
                        <div id="3" class="d-none">
                            <h1 class="titre-slider mt-auto" id="s1" >SUZUKI</h1>
                            <!-- <h2 class="sous-titre-slider" >UX/UI design, Mobile App, Wifi</h2> -->
                            <p class="desc-slider mt-4" >
                                Suzuki, nous a confié la digitalisation de la relation client à travers les prestations suivantes:
                                <ul>
                                    <li>Conception des spécifications fonctionnelles et techniques </li>
                                    <li>Conception UX/UI Développement d'applications (ReactNative) </li>
                                    <li>Installation et configuration du BackOffice Développement d'API </li>
                                    <li>Durée 4 Mois </li>
                                </ul>
                            </p>
                            <a  href="" class="link-slider mb-5">
                                <!-- Case study <img  class="ml-3" src="{{URL::asset('/assets/images/arrow.svg')}}">  -->
                            </a>
                        </div>
                    </div>
                    <!-- Carousel 4 (SUZUKI) -->


                    <!-- <div> <img src="assets/images/slider1.svg" class="w-100" alt="..."> </div> -->
                </div>

            </div>
        </div>
        <!-- End Section slider -->
    </div>

    <!-- Begin Section clients -->
    <div class="py-0 py-md-5">
        <div class="row py-5">

            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/Porsche.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/Audi.svg">
            </div>

            <div class="col-6 col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img class="image-client m-auto" src="assets/images/clients/g5.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/icao.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img class="image-client m-auto" src="assets/images/clients/modo.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/auto+.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img class="image-client m-auto" src="assets/images/clients/cpscl.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/gam.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/bee.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img class="image-client m-auto" src="assets/images/clients/tae.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/artisan.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img class="image-client m-auto" src="assets/images/clients/prisma.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/stb.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img class="image-client m-auto" src="assets/images/clients/avene.svg">
            </div>


            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img class="image-client m-auto" src="assets/images/clients/baguette.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/suzuki.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img class="image-client m-auto" src="assets/images/clients/geiser.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/carrefour.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img class="image-client m-auto" src="assets/images/clients/geant.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/peugeot.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/indigo.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img class="image-client m-auto" src="assets/images/clients/total.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/ministere.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img class="image-client m-auto" src="assets/images/clients/usa.svg">
            </div>
            <div class="col-6 col-md-2 col-lg-2 d-flex mb-3">
                <img  class="image-client m-auto" src="assets/images/clients/mos.svg">
            </div>



        </div>
    </div>
    <!-- End Section clients -->
</section>

<!-- End Section Three Xelero -->

<!-- Begin Section Four Xelero -->
    <!-- <section class="section-four" id="section-four">
        <div class="container-lg py-3">
            <div class="row py-5  justify-content-md-between  justify-content-center  ">
                <div class="col-md-12 col-lg-12 d-flex mb-3 ">
                    <p class="titre-section-two my-0 my-md-5" >Nos partenaires</p>
                </div>
                <div class="col-10 col-lg-3 d-flex flex-column py-5">
                        <img src="assets/images/xelero-p.svg">
                        <h5 class="my-4" >Partenaire</h5>
                        <p>Lorem ipsum dolor sit amet, cum rebum deserunt partiendo te,</p>
                </div>
                <div class="col-10 col-lg-3 d-flex flex-column py-5">
                        <img src="assets/images/xelero-p.svg">
                        <h5 class="my-4" >Partenaire</h5>
                        <p>Lorem ipsum dolor sit amet, cum rebum deserunt partiendo te,</p>
                </div>
                <div class="col-10 col-lg-3 d-flex flex-column py-5">
                        <img src="assets/images/xelero-p.svg">
                        <h5 class="my-4" >Partenaire</h5>
                        <p>Lorem ipsum dolor sit amet, cum rebum deserunt partiendo te,</p>
                </div>
            </div>
        </div>
    </section> -->
<!-- End Section Four Xelero -->

<section>
    <div class="head-footer" >
        <div class="container py-3">
            <div class="row py-5">
                <div class="col-9 col-lg-8 d-flex flex-column">
                    <h1 class="mb-5" >Démarrons votre projet <br> innovant  ensemble, parlez nous de votre besoin !</h1>
                    <p>
                        Toute notre équipe est à votre écoute, n’hésitez pas à nous contacter                
                    </p>
                </div>
                <div class="col-3 col-lg-4 d-flex p-0 p-md-auto" id="box-footer-arrow" >
                    <img src="{{URL::asset('assets/images/footer-arrow.svg')}}" class="mx-auto arrow-footer"  id="footer-arrow"  >
                    <img src="{{URL::asset('assets/images/footer-arrow-hover.svg')}}" class="mx-auto arrow-footer" id="footer-arrow-hover" style="display:none" >
                </div>
                <!-- <div class="col-lg-12">
                    <p>
                        Lorem ipsum dolor sit amet, cum rebum deserunt partiendo te, amet <br> pertinacia contentiones ut sea. 
                    </p>
                </div> -->
                <div class="col-lg-12">
                    <hr>
                </div>
                <div class="col-lg-12">
                    <div class="row  justify-content-between">
                        <div class="col-md-6 col-lg-6 d-flex flex-column">
                            <label class="my-1 my-md-4" >Email</label>
                            <a  class="" href="mailto:Contact@xelero.io">Contact@xelero.io</a>
                        </div>
                        <div class="col-md-4 col-lg-3 d-flex flex-column">
                            <label class="mt-4 mb-1 mt-md-4 mb-md-4 " >Phone</label>
                            <a  class="" href="tel:+216 31 558 906">+216 31 558 906</a>
                        </div>
                    </div>
                </div> 

            </div>
        </div>
    </div>
</section>
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="assets/js/home.js"></script>

