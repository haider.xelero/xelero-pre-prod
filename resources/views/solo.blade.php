
@extends('layouts.main')
@section('solo')

@if(session()->has('message'))
    <script>
        Swal.fire(
            'Good job!',
            'Votre candidature est envoyée avec succès',
            'success'
        )
    </script>
@endif
@if(session()->has('error'))
    <script>
        Swal.fire(
            '',
            'Votre candidature est déjà envoyée',
            'error'
        )
    </script>
@endif
<section>
    <div class="solo">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="titre-solo">
                        {{$job->titre}}
                    </h1>
                    <div class="header-job mt-3">
                        <img src="{{URL::asset('assets/images/clients/location_white.svg')}}">
                        <p class="location">
                            {{$job->location}}
                        </p>
                    </div>
                    <p class="years-exp mt-5">
                        Années d'expérience :  {{$job->annees_experiences_min}} @if ((isset($job->annees_experiences_min)) && isset(($job->annees_experiences_max))) - @endif {{$job->annees_experiences_max}}
                    </p>
                </div>

                <div class="col-lg-6 d-flex flex-column-reverse flex-md-column">
                    <a href="#postuler" id="button-postuler" class="button-contact px-5 ml-md-auto mt-3" >
                        Postuler maintenant
                    </a>
                    <p class="years-exp ml-md-auto mt-auto">Expire le : {{$job->date_cloture}} </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-4 d-flex flex-column">
        <h2 class="titreDescription">
            Description
        </h2>
        <div class="contentDescription mt-4" >
            {!! $job->description !!}
        </div>
        @if(isset($job->qualities_qualifications))
            <h2 class="titreDescription">
                Technologies/Compétences attendues
            </h2>
            <div class="contentDescription mt-4" >
                {!! $job->qualities_qualifications !!}
            </div>
        @endif
    </div>
    <div id="postuler">
        <div class="container my-5">
            <hr>
            <div class="modal-body px-0 py-4">
                <h2 class="titre-apply mb-5">Postuler maintenant</h2>
                <form method="post" action="/insertCondidature" enctype="multipart/form-data">
                {{csrf_field()}}
                    <input type="text" value="{{$job->id}}" name="job_id" hidden/>
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <label class="label-carriere" >
                                Prénom
                                <input type="text" name="prenom" class="input-carriere" required >
                            </label>
                        </div>
                        <div class="col-12 col-lg-6">
                            <label class="label-carriere" >
                                Nom
                                <input type="text" name="nom" class="input-carriere" required >
                            </label>
                        </div>
                        <div class="col-12 col-lg-12">
                            <label class="label-carriere" >
                                Email
                                <input type="text" name="email" class="input-carriere" required >
                            </label>
                        </div>
                        <div class="col-12 col-lg-12">
                            <label class="label-carriere" >
                                Lettre de motivation
                                <textarea class="input-carriere" name="letter" style="height:150px" required  ></textarea>
                            </label>
                        </div>
                        <div class="col-12 col-lg-12">
                            <!--<div class="alert alert-success" id="alert" role="alert" style="display:none;text-align: center;" >
                                Merci de télécharger votre CV
                            </div> -->
                            <div class="alert alert-danger" id="alert" role="alert" style="display:none;text-align:center;" >
                                Merci de télécharger votre CV
                            </div>
                            <label for="file-upload" class="custom-file-upload d-flex mt-3">
                                <div class="d-flex m-auto w-75"  style="position:relative">
                                    <img src="{{URL::asset('assets/images/file.svg')}}" class="my-auto ml-auto mr-3 file-icon">
                                    <p class="m-auto" id="resumeFile" >Attacher votre CV</p>
                                </div>
                            </label>
                            <input id="file-upload" name="cv" type="file"  class="d-none" accept=".pdf"  required/>
                        </div>
                        <div class="col-12 col-lg-12 d-flex mt-3">
                            <button  class="button-contact px-5 m-auto" id="send" >Envoyer</button>
                        </div>
                    </div>
                    <input type="hidden" name="_method" value="PUT">
                </form>
            </div>
            <hr>
        </div>
    </div>
</section>
@endsection
