var name = "" ;

$( "#file-upload" ).click(function() {
    document.getElementById('resumeFile').innerHTML = "Attacher votre CV";
    name = "" ;
});

$( "#file-upload" ).change(function() {
    var fileInput = null ;
    $("#alert").fadeOut(500);
    fileInput = document.getElementById('file-upload');
    var fileName = fileInput.files[0].name.split(".")
    name = document.getElementById('resumeFile').innerHTML =  fileName[0].substr(0,20)+'.'+fileName[1] ;
});


$( "#send" ).click(function() {
    if(name == "") {
        $("#alert").fadeIn(500);
    }
});
