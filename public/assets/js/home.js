var test = null ;
var $j = jQuery.noConflict();

 $j(document).ready(function(){

    console.log("window.screen.width",window.screen.width) ;

    $('.owl-carousel').owlCarousel({
        onChanged: callback,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        margin:10,
        loop:true,
        autoWidth:true,
        items:1,
        dotsContainer: "#dotsOwl",
    })

    function callback(event) {
        test = $("#"+event.page.index).html(); 
        $("#block").html(test);
    }

   if (window.screen.width <= 768) {
    document.getElementById("image-home-desktop").style.display = "none";
    document.getElementById("image-home-mobile").style.display = "block";
   } 
 });

