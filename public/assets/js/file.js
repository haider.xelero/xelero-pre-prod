$(document).ready
(
    function()
    {
        $("#send").click
        (
            function (e)
            {
                var size = $('#file-upload')[0].files[0].size;
                if(size > 2000000)
                {
                    Swal.fire
                    (
                        '',
                        'La taille du fichier ne doit pas dépasser 2 MO',
                        'error'
                    );
                    e.preventDefault();
                }
            }
        );
    }
);

