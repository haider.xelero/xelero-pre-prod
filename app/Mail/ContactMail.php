<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->from($address = 'noreply@domain.com', $name = 'no-reply')
           //         ->subject('Message de Contact')
             //       ->view('myContactEmail')->with(['details' => $this->details]);

                    return $this->view('myContactEmail')
                    //->from('no-reply@xelero.io')
					->from($this->details['email'])
                    ->subject('Contact')
                    
                    ->with([
                        'details' => $this->details,
                    ]);
    }
}
