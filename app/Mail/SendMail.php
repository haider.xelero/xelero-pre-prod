<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $user ;
    protected $job ;

    public function __construct($user,$job)
    {
        $this->user = $user ;
        $this->job = $job ;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail')
            ->from('no-reply@xelero.io')
            ->subject('Candidature '.$this->job)
            ->attach($this->user->cv, [ //absolute path
                'as' => $this->user->nom.'-'.$this->user->prenom.'.pdf',
                'mime' => 'application/pdf'
            ])
            //->attachData($this->user->cv, $this->user->nom.'-'.$this->user->prenom.'.pdf', [
            //    'mime' => 'application/pdf',
            //])
            ->with([
                'user' => $this->user,
                'job' => $this->job,
            ]);
    }
}
