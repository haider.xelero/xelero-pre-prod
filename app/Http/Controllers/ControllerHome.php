<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Mail\SendMailCondidatureSpon;
use App\Mail\SendMail;
use PHPUnit\Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Job;
use App\Message;
use App\CandidatureSpontanee;
use App\Condidature;
use App\Contact;

class ControllerHome extends Controller
{

    public function contact()
    {
        return view('contact');
    }

    public function storeContact(Request $request)
    {
        try
        {
            //return $request;
            $recaptcha = $request->input('g-recaptcha-response');

            if(isset($recaptcha))
            {
                $contactIsExist = Contact::where('email',$request->email)->first();
                //if(!isset($contactIsExist))
                {
                    $condidature = new Contact();
                    $condidature->nom = $request->nom;
                    $condidature->prenom = $request->prenom;
                    $condidature->email = $request->email;
                    $condidature->telephone = $request->tel;
                    $condidature->objet = $request->objet;
                    $condidature->message = $request->letter;
                    $condidature->save();

                    // Send mail to user
                    //$tab = [$request->email,'contact@xelero.io'];
                    //Mail::to($tab)->send(new SendMail($condidature,$condidature->objet));
                    $details = 
                    [
                        'objet' => $request->objet,
                        'message' => $request->letter,
						'email' => $request->email
                    ];
           
                    \Mail::to('contact@xelero.io')->send(new \App\Mail\ContactMail($details));

                    return redirect()->back()->with('message', 'Votre demande a été enregistrée');
                }
            }
            else
            {
                return redirect()->back()->with('error', 'Veuillez confirmer que vous pas un robot');
            }
        }
        catch(Exception $exception)
        {
            return "Exception";
            return $exception->getMessage();
        }
    }

    public function carriere()
    {
        $date = Carbon::now();
        $listJob = Job::where('status',1)->where('date_cloture','>=',$date)->orderBy('date_publication','desc')->get();
        return view('carriere', ['list' => $listJob]);
    }

    public function TestSendMail()
    {
        Mail::to("jouinihaider1@gmail.com")
            // ->cc($moreUsers)
            // ->bcc($evenMoreUsers)
            ->send(new SendMail("test"));

        return view('mail');
    }

    public function storeFn(Request $req)
    {
        $date = Carbon::now();
        $m = date('F');
        $y = date('Y');
        $verifCandidature = CandidatureSpontanee::where('email',$req->email)->first();
        if(isset($verifCandidature))
        {
            //delete the exist cv
            $myCv = json_decode($verifCandidature->cv);
            //delete the storage
            Storage::disk('local')->delete('public/'.$myCv[0]->download_link);
            //insert in database

            // *********** re-upload cv ************
            $urlImageCV = $req->cv->store('public/candidature-spontanees/'.$m.$y) ;
            $urlImageCV = substr($urlImageCV,7);
            $objCV =["download_link" => $urlImageCV,"original_name" => $_FILES['cv']['name'] ] ;
            $cv[] = $objCV ;
            $cv = json_encode($cv) ;
            $verifCandidature->nom = $req->nom ;
            $verifCandidature->prenom = $req->prenom ;
            $verifCandidature->email = $req->email ;
            $verifCandidature->annees_experience = $req->nb ;
            $verifCandidature->cv = $cv;
            $verifCandidature->created_at = $date ;
            $verifCandidature->save() ;
            // Send mail to client
            $tab = [$req->email,'recrutement@xelero.io'];
            Mail::to($tab)->send(new SendMailCondidatureSpon($verifCandidature));
            return redirect()->back()->with('message', 'IT WORKS!');
        }
        //insert in database
        $condidature = new CandidatureSpontanee();

        // *********** upload cv ************
        $urlImageCV = $req->cv->store('public/candidature-spontanees/'.$m.$y) ;
        $urlImageCV = substr($urlImageCV,7);
        $objCV =["download_link" => $urlImageCV,"original_name" => $_FILES['cv']['name'] ] ;
        $cv[] = $objCV ;
        $cv = json_encode($cv) ;
        $condidature->nom = $req->nom ;
        $condidature->prenom = $req->prenom ;
        $condidature->email = $req->email ;
        $condidature->annees_experience = $req->nb ;
        $condidature->cv = $cv;
        $condidature->created_at = $date ;
        $condidature->save() ;
        // Send mail to client
        $tab = [$req->email,'recrutement@xelero.io'];

        Mail::to($tab)->send(new SendMailCondidatureSpon($condidature));

        return redirect()->back()->with('message', 'IT WORKS!');

        //return  redirect('/') ;
    }

    public function insertCondidature(Request $request)
    {
        try
        {
            $condidatureIsExist = Condidature::where('email',$request->email)->where('job_id',$request->job_id)->first();
            if(!isset($condidatureIsExist))
            {
                $fileName = time().'_'.$request->cv->getClientOriginalName();
                $filePath = $request->file('cv')->storeAs('uploads', $fileName, 'public');
                asset('storage/'.$filePath);
                $condidature = new Condidature();
                $condidature->prenom = $request->prenom;
                $condidature->nom = $request->nom;
                $condidature->email = $request->email;
                $condidature->lettre_de_motivation	= $request->letter;
                $cv = '[{"download_link":"'.$filePath.'","original_name":"'.$fileName.'"}]';
                $condidature->cv = $cv;
                $condidature->job_id = $request->job_id;
                $condidature->save();
                $content = json_decode($condidature->cv);
                $condidature->cv = (asset('storage/'.$content[0]->download_link));
                // Send mail to user
                $job = Job::find($condidature->job_id);
                $tab = [$request->email,'recrutement@xelero.io'];
                Mail::to($tab)->send(new SendMail($condidature,$job->titre));


                return redirect()->back()->with('message', 'IT WORKS!');
            }
            return redirect()->back()->with('error', 'Vous avez déjà envoyé votre demande');
        }
        catch(Exception $exception)
        {
            return $exception->getMessage();
        }
    }

    public function solo($id, $titre)
    {
        $job = Job::find($id);
        return view('solo',['job' => $job]);
    }

    public function sendMessage(Request $request)
    {
        try
        {
            $message = new Message();
            $message->prenom = $request->prenom;
            $message->nom = $request->nom;
            $message->email = $request->email;
            $message->message = $request->message;
            $message->save();

            $user = Condidature::where('email',$request->email)->first();

            // Mail::to($request->email)->send(new SendMail($user,));

            return true;
        }
        catch (Exception $exception)
        {
            return $exception->getMessage();
        }
    }

}